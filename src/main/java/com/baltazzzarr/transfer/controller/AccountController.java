package com.baltazzzarr.transfer.controller;


import com.google.inject.Singleton;
import spark.Route;

@Singleton
public class AccountController {
    public Route getAccount() {
        return ((request, response) -> "");
    }

    public Route getAccountTransfersIncoming() {
        return ((request, response) -> "");
    }

    public Route getAccountTransfersOutgoing() {
        return (((request, response) -> ""));
    }
}
