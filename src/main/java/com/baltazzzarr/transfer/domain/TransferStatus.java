package com.baltazzzarr.transfer.domain;

public enum TransferStatus {
    SUCCESS,
    INSUFFICIENT_FUNDS
}
