package com.baltazzzarr.transfer.domain;

public enum Currency {
    USD,
    EUR,
    RUB,
    KZT
}
