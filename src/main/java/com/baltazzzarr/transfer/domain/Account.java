package com.baltazzzarr.transfer.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.math.BigDecimal;

@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
@Table(name = "account")
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @EqualsAndHashCode.Include
    private long id;
    private long ownerId;
    @Enumerated(EnumType.STRING)
    private Currency currency;
    private BigDecimal balance;
    @Version
    private long version;
}
