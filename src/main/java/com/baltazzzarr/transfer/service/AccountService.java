package com.baltazzzarr.transfer.service;

import com.baltazzzarr.transfer.domain.Account;
import com.baltazzzarr.transfer.domain.Currency;

import java.math.BigDecimal;

public interface AccountService {
    Account createAccount(long ownerId, Currency currency, BigDecimal initialBalance);
    Account findById(long id);
}
