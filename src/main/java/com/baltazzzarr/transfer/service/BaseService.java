package com.baltazzzarr.transfer.service;

import com.google.inject.Inject;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.resource.transaction.spi.TransactionStatus;

import java.util.function.Supplier;

@Slf4j
public class BaseService {

    @Inject
    private SessionFactory sessionFactory;

    protected void doInTransaction(Runnable runnable) {
        Supplier<Void> supplier = () -> {
            runnable.run();
            return null;
        };
        withTransaction(supplier);
    }

    protected <R> R withTransaction(Supplier<R> supplier) {
        Session session;
        Transaction transaction = null;
        R result;
        boolean isExternalTransaction = false;
        try {
            session = sessionFactory.getCurrentSession();
            transaction = session.getTransaction();
            if (!transaction.isActive()) {
                transaction.begin();
                isExternalTransaction = true;
            }
            result = supplier.get();
            if (isExternalTransaction) {
                transaction.commit();
            }
        } catch (Exception e) {
            if (transaction != null && isExternalTransaction) {
                if (transaction.getStatus() != TransactionStatus.ROLLED_BACK) {
                    transaction.rollback();
                }
            }
            throw new RuntimeException(e);
        }
        return result;
    }
}
