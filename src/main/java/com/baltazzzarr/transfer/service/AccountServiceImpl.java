package com.baltazzzarr.transfer.service;

import com.baltazzzarr.transfer.domain.Account;
import com.baltazzzarr.transfer.domain.Currency;
import com.baltazzzarr.transfer.exception.DataAccessRuntimeException;
import com.baltazzzarr.transfer.exception.NotFoundRuntimeException;
import com.baltazzzarr.transfer.repository.AccountRepository;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.hibernate.Transaction;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.Optional;

@Singleton
public class AccountServiceImpl extends BaseService implements AccountService {

    @Inject
    private AccountRepository accountRepository;

    @Override
    public Account createAccount(long ownerId, Currency currency, BigDecimal initialBalance) {
        return withTransaction(() -> {
            Account account = new Account();
            account.setOwnerId(ownerId);
            account.setCurrency(currency);
            account.setBalance(initialBalance);
            accountRepository.save(account);
            return account;
        });
    }

    @Override
    @Transactional
    public Account findById(long id) {
        Optional<Account> account = withTransaction(() -> accountRepository.findById(id));
        return account
                .orElseThrow(() -> new NotFoundRuntimeException("Cannot find account with id=" + id));
    }
}
