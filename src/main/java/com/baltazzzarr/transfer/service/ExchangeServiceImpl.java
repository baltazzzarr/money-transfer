package com.baltazzzarr.transfer.service;

import com.baltazzzarr.transfer.domain.Currency;
import com.google.inject.Singleton;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@Singleton
public class ExchangeServiceImpl implements ExchangeService {

    private Map<CurrencyPair, BigDecimal> rates = new HashMap<>();

    public ExchangeServiceImpl() {
        rates.put(new CurrencyPair(Currency.RUB, Currency.EUR), BigDecimal.valueOf(78.9));
        rates.put(new CurrencyPair(Currency.RUB, Currency.USD), BigDecimal.valueOf(68.2));
        rates.put(new CurrencyPair(Currency.RUB, Currency.KZT), BigDecimal.valueOf(0.19));
        rates.put(new CurrencyPair(Currency.USD, Currency.EUR), BigDecimal.valueOf(1.15));
        rates.put(new CurrencyPair(Currency.KZT, Currency.USD), BigDecimal.valueOf(368.79));
        rates.put(new CurrencyPair(Currency.KZT, Currency.EUR), BigDecimal.valueOf(425.35));
    }

    @Override
    public BigDecimal getExchangeRate(Currency source, Currency target) {
        if (source == target) {
            return BigDecimal.ONE;
        }
        CurrencyPair key = new CurrencyPair(source, target);
        return rates.getOrDefault(key, rates.getOrDefault(key.inverse(), BigDecimal.ONE));
    }


    @AllArgsConstructor
    @EqualsAndHashCode
    private static class CurrencyPair {
        @Getter
        private Currency from;

        @Getter
        private Currency to;

        CurrencyPair inverse() {
            return new CurrencyPair(to, from);
        }
    }
}
