package com.baltazzzarr.transfer.service;

import com.baltazzzarr.transfer.domain.Currency;

import java.math.BigDecimal;

/**
 * Created by nkurtushin on 02.09.2018.
 */
public interface ExchangeService {
    BigDecimal getExchangeRate(Currency source, Currency target);
}

