package com.baltazzzarr.transfer.service;

import com.baltazzzarr.transfer.domain.Currency;
import com.baltazzzarr.transfer.domain.Transfer;

import java.math.BigDecimal;

public interface TransferService {
    Transfer doTransfer(long sourceAccountId, long targetAccountId, Currency currency, BigDecimal moneyAmount);
}
