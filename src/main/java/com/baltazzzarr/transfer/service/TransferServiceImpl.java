package com.baltazzzarr.transfer.service;

import com.baltazzzarr.transfer.domain.Account;
import com.baltazzzarr.transfer.domain.Currency;
import com.baltazzzarr.transfer.domain.Transfer;
import com.baltazzzarr.transfer.domain.TransferStatus;
import com.baltazzzarr.transfer.exception.TransferException;
import com.baltazzzarr.transfer.repository.TransferRepository;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;

@Singleton
@Slf4j
public class TransferServiceImpl extends BaseService implements TransferService {

    @Inject
    private TransferRepository transferRepository;

    @Inject
    private AccountService accountService;

    @Inject
    private ExchangeService exchangeService;

    @Override
    public Transfer doTransfer(long sourceAccountId, long targetAccountId,
                               Currency currency, BigDecimal transferMoneyAmount) throws TransferException {
        Transfer transfer = new Transfer();
        transfer.setAmount(transferMoneyAmount);
        transfer.setCurrency(currency);

        try {
            return withTransaction(() -> {
                transfer.setSource(accountService.findById(sourceAccountId));
                transfer.setTarget(accountService.findById(targetAccountId));
                if (!hasEnoughMoney(transfer.getSource(), currency, transferMoneyAmount)) {
                    transfer.setStatus(TransferStatus.INSUFFICIENT_FUNDS);
                } else {
                    executeTransfer(transfer);
                    transfer.setStatus(TransferStatus.SUCCESS);
                }
                transferRepository.save(transfer);
                return transfer;
            });
        } catch (Exception e) {
            log.error("Cannot execute money transfer", e);
            throw new TransferException(e);
        }
    }

    private void executeTransfer(Transfer transfer) {
        Account source = transfer.getSource();
        Account destination = transfer.getTarget();
        BigDecimal transferRate = exchangeService.getExchangeRate(transfer.getCurrency(), source.getCurrency());
        BigDecimal withdrawAmount = transfer.getAmount().multiply(transferRate);
        BigDecimal accountsRate = exchangeService.getExchangeRate(source.getCurrency(), destination.getCurrency());
        BigDecimal depositAmount = withdrawAmount.multiply(accountsRate);
        source.setBalance(source.getBalance().subtract(withdrawAmount));
        destination.setBalance(destination.getBalance().add(depositAmount));
    }

    private boolean hasEnoughMoney(Account account, Currency transferCurrency, BigDecimal transferMoneyAmount) {
        BigDecimal rate = exchangeService.getExchangeRate(account.getCurrency(), transferCurrency);
        BigDecimal accountBalanceInTransferCurrency = account.getBalance().multiply(rate);
        return accountBalanceInTransferCurrency.compareTo(transferMoneyAmount) >= 0;
    }
}
