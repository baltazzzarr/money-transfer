package com.baltazzzarr.transfer.repository;

import com.baltazzzarr.transfer.domain.Transfer;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.Optional;

@Singleton
public class TransferRepositoryImpl implements TransferRepository {

    private SessionFactory sessionFactory;

    @Inject
    public TransferRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
        System.out.println(sessionFactory);
    }

    @Override
    public Optional<Transfer> findById(Long id) {
        return Optional.of(getCurrentSession().get(Transfer.class, id));
    }

    @Override
    public Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }
}
