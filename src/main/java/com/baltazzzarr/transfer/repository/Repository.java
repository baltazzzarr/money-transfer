package com.baltazzzarr.transfer.repository;

import org.hibernate.Session;

import java.util.Optional;

public interface Repository<T, ID> {
    Optional<T> findById(ID id);
    Session getCurrentSession();

    default void save(T entity) {
        getCurrentSession().saveOrUpdate(entity);
    }

    default void delete(ID id) {
        getCurrentSession().delete(findById(id));
    }

}
