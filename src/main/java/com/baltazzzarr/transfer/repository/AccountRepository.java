package com.baltazzzarr.transfer.repository;

import com.baltazzzarr.transfer.domain.Account;

public interface AccountRepository extends Repository<Account, Long> {

}
