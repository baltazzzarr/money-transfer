package com.baltazzzarr.transfer.repository;

import com.baltazzzarr.transfer.domain.Transfer;

import java.util.UUID;

public interface TransferRepository extends Repository<Transfer, Long> {
}
