package com.baltazzzarr.transfer.repository;

import com.baltazzzarr.transfer.domain.Account;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.Optional;

@Singleton
public class AccountRepositoryImpl implements AccountRepository {
    private SessionFactory sessionFactory;

    @Inject
    public AccountRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
        System.out.println(sessionFactory);
    }

    @Override
    public Optional<Account> findById(Long id) {
        return Optional.ofNullable(getCurrentSession().get(Account.class, id));
    }

    @Override
    public Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }
}
