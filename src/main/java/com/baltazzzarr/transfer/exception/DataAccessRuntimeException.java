package com.baltazzzarr.transfer.exception;

/**
 * Created by nkurtushin on 02.09.2018.
 */
public class DataAccessRuntimeException extends RuntimeException {
    public DataAccessRuntimeException() {
    }

    public DataAccessRuntimeException(String message) {
        super(message);
    }

    public DataAccessRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }
}
