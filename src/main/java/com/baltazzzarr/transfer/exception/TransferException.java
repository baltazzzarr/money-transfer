package com.baltazzzarr.transfer.exception;

/**
 * Created by Nickolay_Kurtushin on 9/4/2018.
 */
public class TransferException extends RuntimeException {
    public TransferException() {
    }

    public TransferException(String message) {
        super(message);
    }

    public TransferException(String message, Throwable cause) {
        super(message, cause);
    }

    public TransferException(Throwable cause) {
        super(cause);
    }
}
