package com.baltazzzarr.transfer.exception;

/**
 * Created by nkurtushin on 02.09.2018.
 */
public class NotFoundRuntimeException extends RuntimeException {
    public NotFoundRuntimeException() {
    }

    public NotFoundRuntimeException(String message) {
        super(message);
    }

    public NotFoundRuntimeException(Throwable cause) {
        super(cause);
    }
}
