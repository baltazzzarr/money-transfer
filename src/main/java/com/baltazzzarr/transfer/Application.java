package com.baltazzzarr.transfer;

import com.baltazzzarr.transfer.controller.AccountController;
import com.baltazzzarr.transfer.controller.TransferController;
import com.baltazzzarr.transfer.domain.Account;
import com.baltazzzarr.transfer.domain.Currency;
import com.baltazzzarr.transfer.domain.Transfer;
import com.baltazzzarr.transfer.repository.AccountRepository;
import com.baltazzzarr.transfer.repository.AccountRepositoryImpl;
import com.baltazzzarr.transfer.repository.TransferRepository;
import com.baltazzzarr.transfer.repository.TransferRepositoryImpl;
import com.baltazzzarr.transfer.service.*;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import static spark.Spark.*;

import java.math.BigDecimal;

public class Application extends AbstractModule {

    public static void main(String[] args) {
        Application application = new Application();
        application.setUp();
        port(8080);
        get("/hello", (request, response) -> "hello");
    }

    private void setUp() {
        Injector injector = Guice.createInjector(new Application());
        AccountService accountService = injector.getInstance(AccountService.class);
        TransferService transferService = injector.getInstance(TransferService.class);

        Account source = accountService.createAccount(11L, Currency.RUB, BigDecimal.valueOf(11111.0));
        Account target = accountService.createAccount(12L, Currency.RUB, BigDecimal.valueOf(0.5));
        Transfer transfer = transferService.doTransfer(source.getId(), target.getId(), Currency.RUB, BigDecimal.valueOf(150));

        System.out.println(accountService.findById(1L));
    }

    @Override
    protected void configure() {
        bind(SessionFactory.class).toProvider(this::buildSessionFactory)
                .asEagerSingleton();
        bind(AccountRepository.class).to(AccountRepositoryImpl.class);
        bind(AccountService.class).to(AccountServiceImpl.class);
        bind(ExchangeService.class).to(ExchangeServiceImpl.class);
        bind(TransferRepository.class).to(TransferRepositoryImpl.class);
        bind(TransferService.class).to(TransferServiceImpl.class);
        bind(TransferController.class).toInstance(new TransferController());
        bind(AccountController.class).toInstance(new AccountController());
    }

    private SessionFactory buildSessionFactory() {
        StandardServiceRegistry registry = null;
        try {
            registry = new StandardServiceRegistryBuilder()
                    .configure()
                    .build();

            return new MetadataSources(registry)
                    .getMetadataBuilder()
                    .build()
                    .getSessionFactoryBuilder()
                    .build();

        } catch (Exception e) {
            if (registry != null) {
                StandardServiceRegistryBuilder.destroy(registry);
            }
            throw new ExceptionInInitializerError();
        }
    }

}
